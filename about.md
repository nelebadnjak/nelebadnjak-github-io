---
layout: page
title: About 
permalink: /about/
tags: about
---

Hello! 
I am Nebojša Badnjar, SysOp from Novi Sad, Serbia. This blog is cheatsheet for myself and it is powered by Jekyll. Here you can find things related to my daily work.

Feel free to [find](https://www.nelebadnjak.com/) me. 

Cheers!